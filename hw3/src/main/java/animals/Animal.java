package animals;

import food.Food;

import java.util.Objects;

public abstract class Animal {
    private String name = this.getClass().getSimpleName();

    public abstract SizeCage getSizeCage();

    public boolean setName(String name) {
        if (name != null) {
            this.name = name;
            return true;
        } else {
            return false;
        }
    }

    public String getName(){
        return this.name;
    };

    public abstract boolean eat(Food food) throws WrongFoodException;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Animal)) return false;
        Animal animal = (Animal) o;
        return getName().equals(animal.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
