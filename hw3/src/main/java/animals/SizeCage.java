package animals;

public enum SizeCage {
    SMALL,
    MEDIUM,
    LARGE,
    LARGEST
}
