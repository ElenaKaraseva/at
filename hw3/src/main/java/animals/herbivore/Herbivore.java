package animals.herbivore;

import animals.Animal;
import animals.SizeCage;
import animals.WrongFoodException;
import food.Food;
import food.Meat;

public abstract class Herbivore extends Animal {
    public abstract SizeCage getSizeCage();

    public boolean eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            System.out.println(getName() + " eat grass");
            return true;
        } else {
            System.out.println("Herbivore cannot eat meat");
            return false;
        }
    }
}
