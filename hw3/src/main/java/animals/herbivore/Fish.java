package animals.herbivore;

import animals.SizeCage;
import animals.Swim;
import animals.WrongFoodException;
import food.Food;


public class Fish extends Herbivore implements Swim {
    private SizeCage sizeCage = SizeCage.SMALL;

    public SizeCage getSizeCage() {
        return sizeCage;
    }

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public void swim() {
        System.out.println("The " + getName() + " swam");
    }
}
