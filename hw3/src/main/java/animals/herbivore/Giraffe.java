package animals.herbivore;

import animals.*;
import food.Food;

public class Giraffe extends Herbivore implements Run, Swim, Voice {
    private final String voice = "\"Woo\"";
    private SizeCage sizeCage = SizeCage.LARGEST;


    @Override
    public SizeCage getSizeCage() {
        return sizeCage;
    }

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public void run() {
        System.out.println("The " + getName() + " ran");
    }

    @Override
    public void swim() {
        System.out.println("The " + getName() + " swam");
    }

    @Override
    public String voice() {
        return "The " + getName() + " say " + voice;
    }
}
