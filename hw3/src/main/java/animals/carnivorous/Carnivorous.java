package animals.carnivorous;

import animals.Animal;
import animals.SizeCage;
import animals.WrongFoodException;
import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {
    public abstract SizeCage getSizeCage();

    public boolean eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            System.out.println(getName() + " eat meat");
            return true;
        } else {
            System.out.println("Carnivorous cannot eat grass");
            return false;
        }
    }
}

