package animals.carnivorous;

import animals.*;
import food.Food;

public class Lion extends Carnivorous implements Run, Swim, Voice {
    private final String voice = "\"Graaaaaaar\"";
    SizeCage sizeCage = SizeCage.LARGE;

    @Override
    public SizeCage getSizeCage() {
        return sizeCage;
    }

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public void run() {
        System.out.println("The " + getName() + " ran");
    }

    @Override
    public void swim() {
        System.out.println("The " + getName() + " swam");
    }

    @Override
    public String voice() {
        return "The " + getName() + " roars " + voice;
    }
}
