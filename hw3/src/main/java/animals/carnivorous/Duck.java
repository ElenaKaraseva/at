package animals.carnivorous;

import animals.*;
import food.Food;

public class Duck extends Carnivorous implements Fly, Run, Swim, Voice {
    private final String voice = "\"Quack-quack\"";
    SizeCage sizeCage = SizeCage.MEDIUM;

    @Override
    public SizeCage getSizeCage() {
        return sizeCage;
    }

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public void fly() {
        System.out.println("The " + getName() + " flew over");
    }

    @Override
    public void run() {
        System.out.println("The " + getName() + " ran");
    }

    @Override
    public void swim() {
        System.out.println("The " + getName() + " swam");
    }

    @Override
    public String voice() {
        return "The " + getName() + " quacks " + voice;
    }
}
