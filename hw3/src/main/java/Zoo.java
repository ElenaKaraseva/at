import animals.SizeCage;
import animals.Swim;
import animals.carnivorous.Duck;
import animals.carnivorous.Lion;
import animals.carnivorous.Wolf;
import animals.herbivore.Fish;
import animals.herbivore.Giraffe;
import animals.herbivore.Herbivore;
import animals.herbivore.Horse;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {

        Duck duck = new Duck();
        duck.setName("Scrooge McDuck");
        Lion lion = new Lion();
        Wolf wolf = new Wolf();
        Fish fish = new Fish();
        fish.setName("Nemo");
        Giraffe giraffe = new Giraffe();
        Horse horse = new Horse();
        Grass grass = new Grass();
        Meat meat = new Meat();
        Worker worker = new Worker();

        worker.feed(duck, meat);
        worker.feed(duck, grass);
        worker.feed(lion, meat);
        worker.feed(lion, grass);
        worker.feed(wolf, meat);
        worker.feed(wolf, grass);
        worker.feed(fish, meat);
        worker.feed(fish, grass);
        worker.feed(giraffe, meat);
        worker.feed(giraffe, grass);
        worker.feed(horse, meat);
        worker.feed(horse, grass);
        System.out.println("\n");

        worker.getVoice(duck);
        worker.getVoice(lion);
        worker.getVoice(wolf);
        worker.getVoice(giraffe);
        worker.getVoice(horse);
        System.out.println("\n");

        Swim[] pond = new Swim[4];
        pond[0] = new Duck();
        pond[1] = new Duck();
        pond[2] = new Fish();
        pond[3] = new Fish();
        for (int i = 0; i < pond.length; i++) {
            pond[i].swim();
        }
        System.out.println("\n");

        Cage<Lion> cage1 = new Cage(SizeCage.LARGEST);
        cage1.addAnimal(lion);
        //cage1.addAnimal(giraffe);
        cage1.removeAnimal(lion);
        System.out.println("Size of cage1: " + cage1.numberOfAnimals());

        Cage<Herbivore> cageHerbivore = new Cage(SizeCage.LARGEST);
        cageHerbivore.addAnimal(horse);
        cageHerbivore.addAnimal(fish);
        System.out.println("Size of cage1: " + cageHerbivore.numberOfAnimals());
    }
}
