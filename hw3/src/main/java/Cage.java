import animals.Animal;
import java.util.HashSet;
import java.util.Set;
import animals.SizeCage;

public class Cage<T extends Animal>{
    private Set<T> cage;
    private SizeCage sizeCage;

    public Cage(SizeCage sizeCage) {
        cage = new HashSet<>();
        this.sizeCage = sizeCage;
        System.out.println("The cage has been created");
    }

    public void addAnimal(T animal) {
        int sizeAnimal = animal.getSizeCage().ordinal();
        int sizeCreatedCage = this.sizeCage.ordinal();
        if (sizeAnimal <= sizeCreatedCage) {
            cage.add(animal);
            System.out.println("The " + animal.getName() + " is in the cage");
        } else {
        System.out.println("This cage is too small");
        }
    }

    public int numberOfAnimals() {
        return cage.size();
    }

    public void removeAnimal(T animal) {
        this.cage.remove(animal);
        System.out.println("Delete:" + animal.getName());
    }
}