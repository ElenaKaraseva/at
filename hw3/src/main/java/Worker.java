import animals.Animal;
import animals.Voice;
import animals.WrongFoodException;
import food.Food;

public class Worker {

    public boolean feed(Animal animal, Food food) {
        try {
            animal.eat(food);
        } catch (WrongFoodException wfExp) {
            System.out.println("The food is inedible." + " Animal: " + animal.getName());
        }
        return true;
    }

    public void getVoice(Voice animalVoice) {
        System.out.println(animalVoice.voice());
    }
}
