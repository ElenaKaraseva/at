import animals.Animal;
import animals.Voice;
import food.Food;

import java.util.ArrayList;

public class Worker {

    public boolean feed(Animal animal, Food food) {
        animal.eat(food);
        return true;
    }

    public void getVoice(Voice animalVoice) {
        System.out.println(animalVoice.voice());
    }
}
