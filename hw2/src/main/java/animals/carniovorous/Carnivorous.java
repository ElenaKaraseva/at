package animals.carniovorous;

import animals.Animal;
import animals.Voice;
import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {
    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    public boolean eat(Food food) {
        if (food instanceof Meat) {
            System.out.println(getName() + " eat meat");
            return true;
        } else {
            System.out.println("Carnivorous cannot eat grass");
            return false;
        }
    }
}
