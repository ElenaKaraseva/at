package animals.carniovorous;

import animals.Run;
import animals.Swim;
import animals.Voice;
import food.Food;

public class Wolf extends Carnivorous implements Run, Swim, Voice {
    private final String voice = "\"owooooooo\"";

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public boolean eat(Food food) {
        return super.eat(food);
    }

    @Override
    public void run() {
        System.out.println(getName() + " ran");
    }

    @Override
    public void swim() {
        System.out.println(getName() + " swam");
    }

    @Override
    public String voice() {
        return getName() + " howls " + voice;
    }
}
