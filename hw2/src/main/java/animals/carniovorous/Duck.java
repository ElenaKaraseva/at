package animals.carniovorous;

import animals.Fly;
import animals.Run;
import animals.Swim;
import animals.Voice;
import food.Food;

public class Duck extends Carnivorous implements Fly, Run, Swim, Voice {
    private final String voice = "\"Quack-quack\"";

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public boolean eat(Food food) {
        return super.eat(food);
    }

    @Override
    public void fly() {
        System.out.println(getName() + " flew over");
    }

    @Override
    public void run() {
        System.out.println(getName() + " ran");
    }

    @Override
    public void swim() {
        System.out.println(getName() + " swam");
    }

    @Override
    public String voice() {
        return getName() + " quacks " + voice;
    }
}
