package animals.carniovorous;

import animals.Run;
import animals.Swim;
import animals.Voice;
import food.Food;

public class Lion extends Carnivorous implements Run, Swim, Voice {
    private final String voice = "\"Graaaaaaar\"";

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public boolean eat(Food food) {
        return super.eat(food);
    }

    @Override
    public void run() {
        System.out.println(getName() + " ran");
    }

    @Override
    public void swim() {
        System.out.println(getName() + " swam");
    }

    @Override
    public String voice() {
        return getName() + " roars " + voice;
    }
}
