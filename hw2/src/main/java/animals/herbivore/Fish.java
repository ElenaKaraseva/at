package animals.herbivore;

import animals.Swim;
import food.Food;

public class Fish extends Herbivore implements Swim {

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public boolean eat(Food food) {
        return super.eat(food);
    }

    @Override
    public void swim() {
        System.out.println(getName() + " swam");
    }
}
