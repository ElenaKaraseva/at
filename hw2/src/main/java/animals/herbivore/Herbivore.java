package animals.herbivore;

import animals.Animal;
import animals.Voice;
import food.Food;
import food.Grass;
import food.Meat;

public abstract class Herbivore extends Animal {
    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    public boolean eat(Food food) {
        if (food instanceof Grass) {
            System.out.println(getName() + " eat grass");
            return true;
        } else {
            System.out.println("Herbivores cannot eat grass");
            return false;
        }
    }
}
