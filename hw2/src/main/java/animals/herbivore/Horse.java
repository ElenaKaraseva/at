package animals.herbivore;

import animals.Run;
import animals.Swim;
import animals.Voice;
import food.Food;

public class Horse extends Herbivore implements Run, Swim, Voice {
    private final String voice = "\"Neigh-neigh\"";

    public boolean setName(String name) {
        return super.setName(name);
    }

    public String getName() {
        return super.getName();
    }

    @Override
    public boolean eat(Food food) {
        return super.eat(food);
    }

    @Override
    public void run() {
        System.out.println(getName() + " ran");
    }

    @Override
    public void swim() {
        System.out.println(getName() + " swam");
    }

    @Override
    public String voice() {
        return getName() + " neighing " + voice;
    }
}
