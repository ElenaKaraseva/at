package animals;

import food.Food;

public abstract class Animal {
    private String name = this.getClass().getSimpleName();

    public boolean setName(String name) {
        if (name != null) {
            this.name = name;
            return true;
        } else {
            return false;
        }
    }
    //public abstract boolean setName(String name);

    public String getName(){
        return this.name;
    };

    public abstract boolean eat(Food food);
}
