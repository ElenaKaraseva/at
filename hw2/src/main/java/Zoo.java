import animals.Animal;
import animals.Fly;
import animals.Swim;
import animals.Voice;
import animals.carniovorous.Duck;
import animals.carniovorous.Lion;
import animals.carniovorous.Wolf;
import animals.herbivore.Fish;
import animals.herbivore.Giraffe;
import animals.herbivore.Horse;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {

        Duck duck = new Duck();
        duck.setName("Scrooge McDuck");
        Lion lion = new Lion();
        Wolf wolf = new Wolf();
        Fish fish = new Fish();
        fish.setName("Nemo");
        Giraffe giraffe = new Giraffe();
        Horse horse = new Horse();
        Grass grass = new Grass();
        Meat meat = new Meat();
        Worker worker = new Worker();

        worker.feed(duck, meat);
        worker.feed(duck, grass);
        worker.feed(lion, meat);
        worker.feed(lion, grass);
        worker.feed(wolf, meat);
        worker.feed(wolf, grass);
        worker.feed(fish, meat);
        worker.feed(fish, grass);
        worker.feed(giraffe, meat);
        worker.feed(giraffe, grass);
        worker.feed(horse, meat);
        worker.feed(horse, grass);
        System.out.println("\n");

        worker.getVoice(duck);
        worker.getVoice(lion);
        worker.getVoice(wolf);
        worker.getVoice(giraffe);
        worker.getVoice(horse);
        System.out.println("\n");

        Swim[] pond = new Swim[4];
        pond[0] = new Duck();
        pond[1] = new Duck();
        pond[2] = new Fish();
        pond[3] = new Fish();
        for (int i = 0; i < pond.length; i++) {
            pond[i].swim();
        }


    }
}
