import Model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kotik1 = new Kotik();
        Kotik kotik2 = new Kotik(5, "Вася", 5.3, "Мур-мяу");

        kotik1.liveAnotherDay();
        System.out.println();
        System.out.println(kotik1.getName());
        System.out.println(kotik1.getWeight());

        if (kotik1.getMeow().equals(kotik2.getMeow())) {
            System.out.println("Котики разговаривают одинаково");
        } else System.out.println("Котики разговаривают по-разному");

        System.out.println("Количество созданных котов: " + Kotik.getCountOfKotik());
    }
}
