package Model;

import java.util.Random;
import java.util.Scanner;

public class Kotik {
    private int satiety;
    private String name;
    private double weight;
    private int prettiness;
    private String meow;

    private final int SATIETY_DECREASES = 50;
    private final int MAX_OF_FOOD = 1000;
    private static int COUNT_OF_FOOD = 500;
    private static int countOfKotik;

    public Kotik() {
        String name;
        String meow;
        double weight;
        int prettiness;

        Scanner console = new Scanner(System.in);
        System.out.println("Введите имя Вашего кота/кошки:");
        name = console.nextLine();
        System.out.println("Как Ваш кот/кошка разговаривает?");
        meow = console.nextLine();
        System.out.println("Введите вес Вашего кота/кошки в кг (от 0,1 до 20): ");
        weight = console.nextDouble();
        System.out.println("Оцените привлекательность вашего кота/кошки по шкале от 0 до 10:");
        prettiness = console.nextInt();

        setKotik(prettiness, name, weight, meow);
        ++countOfKotik;
    }

    public Kotik(int prettiness, String name, double weight, String meow) {
        setKotik(prettiness, name, weight, meow);
        ++countOfKotik;
    }

    private void setKotik(int prettiness, String name, double weight, String meow) {
        if (prettiness > 10) {
            this.prettiness = 10;
        } else if (prettiness < 0) {
            this.prettiness = 0;
        } else {
            this.prettiness = prettiness;
        }
        this.name = name;
        if (weight > 20) {
            this.weight = 20;
        } else if (weight < 0.1) {
            this.weight = 0.1F;
        } else {
            this.weight = weight;
        }
        this.meow = meow;
    }

    public void liveAnotherDay() {
        satiety = 100;
        for (int i = 0; i < 24; i++) {
            satiety -= SATIETY_DECREASES;
            if (!randomForActions()) {
                catWantsToEat();
                randomForActions();
            }
        }
    }

    public boolean randomForActions() {
        Random k = new Random();
        int num = k.nextInt(5);
        switch (num) {
            case 0:
                if (play()) {
                    return true;
                }
                return false;
            case 1:
                if (sleep()) {
                    return true;
                }
                return false;
            case 2:
                if (chaseMouse()) {
                    return true;
                }
                return false;
            case 3:
                if (catSay()) {
                    return true;
                }
                return false;
            case 4:
                if (sharpenClaws()) {
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    public void eat() {
        Scanner console = new Scanner(System.in);
        System.out.println("Введите, какой едой вы хотите накормить котика?");
        String foodName = console.nextLine();
        System.out.println("Введите, сколько грамм " + foodName + " вы хотите положить?");
        int foodCount = mealsLessThanMaxOfFood(console.nextInt());
        eat(foodCount, foodName);
    }

    private int mealsLessThanMaxOfFood(int foodCount) {
        if (foodCount > MAX_OF_FOOD) {
            foodCount = MAX_OF_FOOD;
        }
        if (foodCount < 0) {
            foodCount = 0;
        }
        return foodCount;
    }

    public void eat(int foodCount) {
        satiety += foodCount;
        System.out.println("Ваш котик съел " + foodCount + " грамм еды");
    }

    public void eat(int foodCount, String foodName) {
        satiety += foodCount;
        System.out.println("Ваш котик съел " + foodCount + " грамм еды под названием: " + foodName + ".");
    }

    private void catWantsToEat() {
        Scanner console = new Scanner(System.in);
        System.out.println("\n" +
                "\nВведи 1, чтобы указать, сколько грамм еды он может сейчас съесть." +
                "\nВведи 2, чтобы указать количество грамм и вид еды" +
                "\nВнимание! Кот не может съесть более " + MAX_OF_FOOD + " грамм за один раз.");
        int num = console.nextInt();
        switch (num) {
            case (1): {
                System.out.println("Введи количество еды в граммах");
                int foodCount = mealsLessThanMaxOfFood(console.nextInt());
                eat(foodCount);
                break;
            }
            case (2): {
                System.out.println("Введи название еды");
                String foodName = console.nextLine();
                System.out.println("Введи количество еды в граммах");
                int foodCount = mealsLessThanMaxOfFood(console.nextInt());
                eat(foodCount, foodName);
                break;
            }
            default:
                eat();
        }
    }

    private boolean mealsLessThanSatietyDecrease(int satiety) {
        if (satiety > SATIETY_DECREASES) {
            return false;
        }
        return true;
    }

    public boolean play() {
        if (mealsLessThanSatietyDecrease(satiety)) {
            System.out.print("Сейчас кот не может играть, он очень голоден. ");
            return false;
        } else {
            System.out.println("Ваш котик играет!");
            return true;
        }
    }

    public boolean sleep() {
        if (mealsLessThanSatietyDecrease(satiety)) {
            System.out.print("Сейчас кот не может спать, он очень голоден. ");
            catWantsToEat();
            return false;
        } else {
            System.out.println("Ваш котик спит...");
            return true;
        }
    }

    public boolean chaseMouse() {
        if (mealsLessThanSatietyDecrease(satiety)) {
            System.out.print("Сейчас кот не может ловить мышь, он очень голоден. ");
            return false;
        } else {
            Random random = new Random();
            int countOfMouse = random.nextInt(5);
            System.out.println("Ваш кот - великолепный охотник! Количетсво пойманных мышей: " + countOfMouse);
            return true;
        }
    }

    public boolean catSay() {
        if (mealsLessThanSatietyDecrease(satiety)) {
            System.out.print("Сейчас кот не хочет с общаться, он очень голоден. ");
            return false;
        } else {
            System.out.println("Котик говорит " + meow);
            return true;
        }
    }

    public boolean sharpenClaws() {
        if (mealsLessThanSatietyDecrease(satiety)) {
            System.out.println("Сейчас кот не хочет точить когти, он очень голоден. ");
            return false;
        } else {
            System.out.println("Кот поточил когти, теперь готов к встрече с мышью.");
            return true;
        }
    }

    public int getSatiety() {
        return satiety;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getMeow() {
        return meow;
    }

    public static int getCountOfKotik() {
        return countOfKotik;
    }
}
